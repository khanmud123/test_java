package myPackage;


public class MyClass {
	
	public void myMethod() {
		
		System.out.println("Hello myMethod");
	}
	
	public static void myStMethod() {
		System.out.println("Hello my static method");
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		myStMethod();
		System.out.println("Hello from inside main method");
		
		MyClass car = new MyClass();
		car.myMethod();
		
		MySecondClass.myLogin();
		
		MySecondClass weblogic = new MySecondClass();
		weblogic.myWebPage();

	}

}


