package myPackage;

public class PerformanceOfStrings {

	public static void main(String[] args) {
		

		String s=new String("Selenium");
		long startTimeOfS=System.currentTimeMillis();
		for(int i=0;i<100000;i++)
		{
			s.concat(" Class");
		}
		long endTimeOfS=System.currentTimeMillis();
		//System.out.println(s);
		System.out.println("Time taken by string is "+(endTimeOfS-startTimeOfS));
		
		StringBuffer sb=new StringBuffer("Selenium");
		long startTimeOfSb=System.currentTimeMillis();
		for(int i=0;i<100000;i++)
		{
			sb.append(" Class");
		}
		long endTimeOfsb=System.currentTimeMillis();
		//System.out.println(sb);
		System.out.println("Time taken by the string buffer is "+(endTimeOfsb-startTimeOfSb));
		
		StringBuilder sb1=new StringBuilder("Selenium");
		long startTimeOfSb1=System.currentTimeMillis();
		for(int i=0;i<100000;i++)
		{
			sb1.append(" Class");
		}
		long endTimeOfsb1=System.currentTimeMillis();
		//System.out.println(sb1);
		System.out.println("Time taken by string builder is "+(endTimeOfsb1-startTimeOfSb1));
		
		
				
	}

}
