package myPackage;

import java.util.Scanner;

public class MyFourthClass {

	public static void main(String[] args) {
		
		//learning overloading
		// TODO Auto-generated method stub
		/*
		 * addition(); addition3(inPutOne(),inPutTwo()); addition2();
		 */
       int a= inPutOne();
       int b= inPutTwo();
              addition3(a,b);
       
	}
	
	 public static void addition()
	   {
		   int a=90;
		   int b=98;
		   System.out.println(a+b);
	   }
	  
	  public static void addition3(int a,int b)
	  {
		  System.out.println(a+b);
	  }
	  
	  public static void addition2()
	  {
		  System.out.println("Input the values for addition 2 method");
		  Scanner scan=new Scanner(System.in);
		   int a1=scan.nextInt();
		   int a2=scan.nextInt();
		   System.out.println(a1+a2);
		  
	  }
	  
	  public static int inPutOne() {
		  
		/*
		 * int a1=30; int a2=40;
		 */
		  int a1=30,a2=40;
		  int c=a1+a2;
		return (c);  
	  }
	  
	  public static int inPutTwo() {
		  
		  int b1=25;
		  int b2=35;
		  return (b1+b2);
	  }
	  
	  public static void addition(int a, int b) {
		  
	  }
	  
	  public static void addition(int a, int b, int c) {
		  
	  }
	  
	  public static void addition (float a, float b, int c, int d) {
		  
	  }
	  
	  

}
