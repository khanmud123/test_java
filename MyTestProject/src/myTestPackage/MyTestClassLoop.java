package myTestPackage;

public class MyTestClassLoop {
	
	public static void main (String[]args) {
		
		int x=11;
		do {
			System.out.println("I am in Do while loop");// 1p,2p,3p,4p,5p,6p,7p
			x--;  
			//10, 9, 8,7,6,5,4
		}
		while (x>=5);
		
		methodFactorial();
			
			
		}
	
	
	
	public static void methodFactorial() {
		int f=1;
		for(int i=5;i>=1;i--) {
			f=f*i;
		}
		
		//1 execution,2nd e, 3rd e, 4th e, 5th e, 
		//1*5=5, 5x4=20, 20x3=60, 60*2=120, 120x
		System.out.println("Factorial of 5 is" +f);
		
	  	
		
		
	}

}

