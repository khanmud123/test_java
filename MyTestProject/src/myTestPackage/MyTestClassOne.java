package myTestPackage;

import java.util.Scanner;

public class MyTestClassOne  {
	
	
	public void myOneTestMethod() {
		// type sysout then ctrl+shift to see
		//whole system.out.println() 
		System.out.println("Hello my one test non static method");
	}
	public static void myTwoTestMethod() {
		System.out.println("Hello my two test method to test static method");
	}
	
	public static void main(String[] args) {
	   //create a object to call non static method inside main method.
       MyTestClassOne a = new MyTestClassOne();
       a.myOneTestMethod();
       a.myTwoTestMethod();
       myTwoTestMethod();
       MyTestClassTwo.loginMethod1();
       System.out.println("write our number");
       Scanner scan=new Scanner(System.in);
       int i= scan.nextInt();
       
       
       if(45>i) {
    	   System.out.println("45 is greater than i");
       }
       else
       {
    	   System.out.println("hellow");
       }
       
       
	}
	    

}
