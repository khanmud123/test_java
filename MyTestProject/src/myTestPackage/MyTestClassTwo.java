package myTestPackage;

public class MyTestClassTwo extends MyTestClassOne {
	public static void loginMethod1() {
		System.out.println("Hello login Method1");
	}
	public void loginMethod2() {
		System.out.println("Hello login Method2");
	}

	public static void main(String[] args) {
		
		//extend: What is the difference between creating object from two different class
		//and call same method with two different object?
		MyTestClassOne b= new MyTestClassOne();
		b.myOneTestMethod();
		
		
		myTwoTestMethod();
		loginMethod1();
		MyTestClassTwo a = new MyTestClassTwo();
		a.loginMethod2();
		a.myOneTestMethod();
	
		
		

	}

}
