package myTestPackage;

public class MyTestOverloadingClass {

	public static void main(String[] args) {
		overloadingMethod();
		overloadingMethod(50, 10);
		overloadingMethod(50, 10, 5);

	}
	static int a;
	static int b;
	static int c;
	static int x=a+b;  
	static int y=a+b+c;
	public static void overloadingMethod() {
		System.out.println("overloadingMethod without any parameter and no return type");
	}
	public static void overloadingMethod(int a,int b) {
		System.out.println(a+b );
	}
	
	public static void overloadingMethod(int a, int b, int c) {
		System.out.println(a+b+c);
	}
	}


