package myTestPackage;

import java.util.Scanner;

public class MyTestSwitchCaseClass {

	public static void main(String[] args) {
		myMethodSwitchCase();
		

	}
	
	public static void myMethodSwitchCase() 
	{
		System.out.println("Enter your number for c1 and c2");
		Scanner scan = new Scanner(System.in);
		int c1= scan.nextInt();
		int c2= scan.nextInt();
	   // String s= scan.next() ;
	    int c3= scan.nextInt();
		// Need to see how int value used instead of string???
	    scan.close();
		switch (c3) 
	   {
		case 1:
			System.out.println("my addition value is:" + (c1+c2));
		//break;
		case 2:
		    System.out.println("my subtraction value is: "+(c1-c2));
		//break;
		case 3:
			System.out.println("my multiplication valie is: " + (c1*c2));
		//break;
		case 4:
			System.out.println("my division value is: " + (c1/c2));
		//break;
		case 5:
			System.out.println("my reminter valie is: " + c1%c2);
			//break;
	   }
		System.out.println("out of switch");
		
	   }

}
