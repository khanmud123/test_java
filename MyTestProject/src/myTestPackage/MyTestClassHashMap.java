package myTestPackage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyTestClassHashMap {

	public static void main(String[] args) {
		methodHashMap();
		methodMapEntry();

	}
	public static void methodHashMap() {
		Map<String, String> phonebook = new HashMap<>();
		phonebook.put("Navin", "7709873456");
		phonebook.put("Ali", "6782345432");
		phonebook.put("Maz", "4049873423");
		//return one key value
		System.out.println("Phone number of Navin is: "+phonebook.get("Navin"));
		//return all key values
		Set<String>Keys = phonebook.keySet();
		
		
	}
	public static void methodMapEntry() {
		Map<String, String> phonebook = new HashMap<>();
		phonebook.put("Navin", "7709873456");
		phonebook.put("Ali", "6782345432");
		phonebook.put("Maz", "4049873423");
		
		Set<Map.Entry<String,String>>values=phonebook.entrySet();
		for(Map.Entry<String, String> e:values) {
			System.out.println(e.getKey() + ":" + e.getValue());
			e.setValue("III");
		}	
			
			
		
	}

}
