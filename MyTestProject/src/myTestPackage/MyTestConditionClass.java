package myTestPackage;

import java.util.Scanner;

public class MyTestConditionClass {

	public static void main(String[] args) {
		
		//methodScannerWithCondition();
		methodNestedIfElse();
		
		
	}
	public static int add(int a,int b)
	{ 
		int c=40,d=40;
	
		return c+b;
		
	}
	public static int sub(int a,int b)
	{
		return -b;
	}
	
	public static void methodScannerWithCondition() {
		System.out.println("Enter number for methodScanner1");
		Scanner scan = new Scanner(System.in);
		int a1=scan.nextInt();
		int a2=scan.nextInt();
		//System.out.println("reminder: " + (a1%a2));
		//System.out.println("division: " + (a1/a2));
		
		if ((a1+a2)>=80) {
			   System.out.println("Students passed with first class");
		   }
		else if ((a1+a2)>=70) {
			System.out.println("Students passed with second class");
		}
		else if (((a1+a2)>50) && ((a1+a2)<70))
		{
			System.out.println("Students take exam");
		}
		else {
			System.out.println("Students take the course again");
		}
	}
	
	public static void methodNestedIfElse() {
		
		
		System.out.println("Enter number for methodNesterIfElse");
		Scanner scan = new Scanner(System.in);
		int b1=scan.nextInt();
		int b2= scan.nextInt();
		
		if (b1>50) 
		{
			System.out.println("Passed in academic");
			if ((b1>50)&& (b2<70)) 
			{
				System.out.println("passed with 3rd class");
			}
			else
				System.out.println("passed with 2nd class");
		 }
		else
		{
			System.out.println("Take the exam");
		}
		}
			 
		
	}
	
   

