package Maven.MavenTestProject;

import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstTestClassWindowHandler {

	public static void main(String[] args) {
	//methodAlertFrameTest();	
	//methodWindowHandlerTest();
	methodAllTogether();
	
	}
	
	public static void methodWindowHandlerTest() {
		
		methodSetProperties();
		//create a new ChromeDriver object by new keyword
		ChromeDriver driver = new ChromeDriver();
		//go to the web site
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		//maximize the web page
		driver.manage().window().maximize();
		//starts work on outside of frame
		driver.findElementByXPath("//a[contains(@class,'rotate')]").click();
		
		//get session id of previous window
		driver.findElementByXPath("//a[contains(@class,'home')]").click();
		String previous_window = driver.getWindowHandle();
		System.out.println("session id of previous window is " + previous_window);
		
		//Work in new tab window
		Set<String> sessionIds = driver.getWindowHandles();
		System.out.println("sessionIds");
		for(String s : sessionIds){
		driver.switchTo().window(s);
		System.out.println("window is shifted to  " + s);  
		}
		
		driver.findElementByXPath("//*[text()='LEARN HTML']").click();
		
		
		
		
	}
	public static void methodAlertFrameTest() {
		methodSetProperties();
		//create a new ChromeDriver object by new keyword
		ChromeDriver driver = new ChromeDriver();
		//go to the website
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		//maximize the web page
		driver.manage().window().maximize();
		//start working on frame
		WebElement iframe =driver.findElementById("iframeResult");
		driver.switchTo().frame(iframe);
		driver.findElementByXPath(".//*[text()='Try it' and not(@class)]").click();
		driver.switchTo().alert().sendKeys("Michael jackson");
		driver.switchTo().alert().accept();
		//get out of frame
		driver.switchTo().defaultContent();
		driver.quit();
	}
	
	
	public static void methodSetProperties() {
		//set the properties of the driver
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	
	}
	
	public static void methodAllTogether() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.findElementByXPath("//a[contains(@class,'home')]").click();
        Set<String> windowhandler = driver.getWindowHandles();
        for(String s: windowhandler) {
        	driver.switchTo().window(s); 
        	System.out.println("previous and present windows session ids are " + windowhandler);// why the system printed twice
        }
        driver.findElementByXPath("//a[contains(text(),'LEARN HTML')]").click();
        }
		
		
	}
	


