package Maven.MavenProject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstAlertClass {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		WebElement frameTryIt=driver.findElementById("iframeResult");
		driver.switchTo().frame(frameTryIt);
		driver.findElementByXPath("/html/body/button").click();
		driver.switchTo().alert().sendKeys("Selenium");
		driver.switchTo().alert().accept();
		

	}

}
