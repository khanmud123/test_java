package Maven.MavenProject;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstClassExceptionHandle {

	public static void main(String[] args)  {
		  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe"); 
		  ChromeDriver driver = new ChromeDriver(); 
		  driver.manage().window().maximize();      
		  driver.get("https://www.facebook.com");                   
		
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			try {
				driver.findElementByName("firstname").sendKeys("Selenium");
				System.out.println("first name entered successfully");
			}
			catch (ElementNotVisibleException e) {
				// TODO: handle exception
				System.err.println("element with the locator value is not visible");
			}
			catch (ElementNotInteractableException e) {
				// TODO: handle exception
				System.err.println("element is not interactabel");
			}
			
			catch (NoSuchElementException e) {
				// TODO: handle exception
				System.err.println("Unabelt to locate the element with the id value of ");
			}
			catch (WebDriverException e) {
				// TODO: handle exception
				System.err.println("web driver excepption");
			}catch (Exception e) {
				// TODO Auto-generated catch block
				System.err.println("Exception occured at the field of first name");
			}
			finally {
				System.out.println("i'm in finally block");
				try {
					File captured=driver.getScreenshotAs(OutputType.FILE);
					File space=new File("./screenshots/snap.jpeg");
					FileUtils.copyFile(captured, space);
				} catch (WebDriverException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			  WebElement lName= driver.findElementByName("lastname");      
			     lName.sendKeys("Training");


	}

}
