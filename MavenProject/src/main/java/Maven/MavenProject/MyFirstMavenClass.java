package Maven.MavenProject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MyFirstMavenClass {

	public static void main(String[] args) {
		
	  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");  // set the property of the driver
	  ChromeDriver driver = new ChromeDriver();  //create a object of chrome driver.
	  driver.manage().window().maximize();       //maximize the window
	 
	  //driver.manage().deleteAllCookies();        // delete all cookies if any left
	  
	  //dynamic wait, need when to use????????
	  //driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
	  //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  
	  driver.get("https://www.facebook.com");                     // go to  facebook page
	  //why I couldn't use Thread, getting error???
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}                                         //wait 5 second
	  try {
		driver.findElementByName("firstname").sendKeys("Selenium");
		System.out.println("i'm in try blcok");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.err.println("firt name is not able to identified");
	} //enter First name
     WebElement lName= driver.findElementByName("lastname");      //find lastname attribut
     lName.sendKeys("Training");                                  //enter lastname 
     //WebElement radio=driver.findElementByName("sex");
     //radio.click();
     //WebElement dropDown = driver.findElementById("month");
     //Select s = new Select(dropDown);
     //s.selectByIndex(6);
     //s.selectByValue("2");
     driver.findElementByName("reg_email__").sendKeys("seleniumtraining083@gmail.com");   //enter email
     driver.findElementByName("reg_email_confirmation__").sendKeys("seleniumtraining083@gmail.com"); //re enter email
     driver.findElementByName("reg_passwd__").sendKeys("Pass#1234"); //enter password
     
     //BirthDay Drop down list
     WebElement mName = driver.findElementById("month");   //drop down month attribute
     WebElement dName = driver.findElementById("day");     //drop down day attribute
     WebElement yName = driver.findElementById("year");    //select drop down year attribute
     Select m = new Select(mName);                         //create a object of Select class
     m.selectByVisibleText("Jan");                         //select the Month attribute by d object
     Select d = new Select(dName);
     d.selectByVisibleText("1");
     Select y= new Select(yName);
     y.selectByVisibleText("1970");
     
     //Gender radio button
     //driver.findElementByName("sex").click();             //select radio button
     //driver.findElementByXPath("//label[text()='Female']//preceding-sibling::input[@name='sex']").click();
     //driver.findElementByXPath("//label[text()=\'Male']//preceding-sibling::input[@name='sex']").click();
     driver.findElementByXPath("//label[text()='Custom']").click();
     
     
     // Your pronoun is visible to everyone.
     driver.findElementByName("preferred_pronoun").click();
     WebElement s =driver.findElementByName("preferred_pronoun");
          Select numberOne= new Select(s);
     //numberOne.selectByVisibleText("She: \"Wish her a happy birthday!\"");
     //driver.findElementByName("preferred_pronoun").click();
     //numberOne.selectByVisibleText("He: \"Wish him a happy birthday!\"");
     //driver.findElementByName("preferred_pronoun").click();
     numberOne.selectByVisibleText("They: \"Wish them a happy birthday!\"");
     driver.findElementByName("preferred_pronoun").click();
     
     //driver.findElementByName("websubmit").click();     //select the submit button for registration.
     
     
	}
	//enterById("firstName","mike");
	public void enterById(String idValue, String text)
	{
		  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");  // set the property of the driver
		  ChromeDriver driver = new ChromeDriver();  //create a object of chrome driver.
		  driver.manage().window().maximize();
		  try {
			driver.findElementById("first").sendKeys(text);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
