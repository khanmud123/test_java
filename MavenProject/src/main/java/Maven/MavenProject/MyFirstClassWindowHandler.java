package Maven.MavenProject;


import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstClassWindowHandler {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");//1
		
		  driver.manage().window().maximize(); 
		  
		 driver.findElementById("tryhome").click();//2
		String window= driver.getWindowHandle();
		System.out.println(window);
		Set<String> sessionIds=driver.getWindowHandles();
		System.out.println(sessionIds);
		for (String s : sessionIds) {
			driver.switchTo().window(s);//s=1st window session id, s=2nd window session id
			System.out.println("Window is shifted to "+s);
	
		}
		 driver.findElementByLinkText("LEARN HTML").click();
		 String window2=driver.getWindowHandle();
		 Set<String> sessionIds2=driver.getWindowHandles();
		 System.out.println(window2);
		 System.out.println(sessionIds2);
	}

}
